package com.kytms.shipmentTemplate.dao.impl;

import com.kytms.core.dao.impl.BaseDaoImpl;
import com.kytms.core.entity.TemplateLedger;
import com.kytms.shipmentTemplate.dao.TemplateLedgerDao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

@Repository(value = "TemplateLedgerDao")
public class TemplateLedgerDaoImpl extends BaseDaoImpl<TemplateLedger> implements TemplateLedgerDao<TemplateLedger> {
    private final Logger log = Logger.getLogger(TemplateLedgerDaoImpl.class);//输出Log日志
}
