package com.kytms.shipmentTemplate.dao;

import com.kytms.core.dao.BaseDao;

public interface TemplateLedgerDTDao <TemplateLedgerDetails>  extends BaseDao<TemplateLedgerDetails> {
}
